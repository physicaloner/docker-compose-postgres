## How to create new db ?

`docker exec -it postgres_db bash`

`psql -h localhost -p 5432 -U postgres`

`CREATE ROLE admin WITH CREATEDB LOGIN PASSWORD 'ppap5432';`

`CREATE DATABASE pineapple OWNER admin;`